import json
from ipaddress import IPv6Network

from django_datacenter_manager_ip.django_datacenter_manager_ip.models import *
from django_datacenter_manager_main.django_datacenter_manager_main.models import *


def generate_dhcp4_config(instance: DcInstance, only_network_ids=None):
    kea_conf = json.load(
        open(
            "django_datacenter_manager_cfg_isc_kea/django_datacenter_manager_cfg_isc_kea/templates/django_datacenter_manager_cfg_isc_kea/kea-dhcp4.conf.json"
        )
    )

    vlans = instance.dcvlan_set.all().select_related()

    kea_conf["Dhcp4"]["option-def"] = list()
    kea_conf["Dhcp4"]["option-data"] = list()
    kea_conf["Dhcp4"]["client-classes"] = list()

    for dhcp_global_default in DcIpDhcpGlobalDefault.objects.all():
        if dhcp_global_default.dhcp_key == "next-server":
            kea_conf["Dhcp4"]["next-server"] = dhcp_global_default.dhcp_value

        if dhcp_global_default.dhcp_key == "boot-file-name":
            kea_conf["Dhcp4"]["boot-file-name"] = dhcp_global_default.dhcp_value

    for vendor_class in instance.dcipdhcpvendorclass_set.all():
        vendor_class_def = dict()

        vendor_class_def["name"] = "vendor-class-" + str(vendor_class.id)
        vendor_class_def["option-def"] = list()
        vendor_class_def["option-data"] = list()

        if vendor_class.dhcp_ipv4_next_server is not None:
            vendor_class_def["next-server"] = vendor_class.dhcp_ipv4_next_server

        if vendor_class.dhcp_ipv4_boot_file_name is not None:
            vendor_class_def["boot-file-name"] = vendor_class.dhcp_ipv4_boot_file_name

        for vendor_class_identifier in vendor_class.dcipdhcpvendorclassmatch_set.all():
            if vendor_class_identifier.vendor_class_substring:
                vendor_class_def["test"] = (
                    "substring(option[{}].hex,{},{}) == '{}'".format(
                        vendor_class_identifier.vendor_class_substring_option_id,
                        vendor_class_identifier.vendor_class_substring_start,
                        vendor_class_identifier.vendor_class_substring_end,
                        vendor_class_identifier.vendor_class_substring_value,
                    )
                )

                continue

            vci = vendor_class_identifier.vendor_class_identifier.decode("ascii")
            vendor_class_def["test"] = (
                "option[vendor-class-identifier].text == '" + vci + "'"
            )

        vendor_class_def["option-data"].append(
            {
                "name": "vendor-encapsulated-options",
            }
        )

        vendor_class_def["option-def"].append(
            {
                "code": 43,
                "encapsulate": "space-code43-" + str(vendor_class.id),
                "name": "vendor-encapsulated-options",
                "type": "empty",
            }
        )

        for vendor_class_option in vendor_class.dcipdhcpvendorclassoption_set.all():
            space_name = "space-" + str(vendor_class.id)
            option_name = "code-" + str(vendor_class_option.option_code)

            data_value = ""

            if vendor_class_option.option_type.name == "binary":
                data_value = vendor_class_option.value.hex()

            if vendor_class_option.option_type.name == "boolean":
                if int(vendor_class_option.value):
                    data_value = "true"
                else:
                    data_value = "false"

            if vendor_class_option.option_type.name == "string":
                data_value = vendor_class_option.value.decode("ascii")

            if vendor_class_option.option_type.name == "ipv4-address":
                data_value = vendor_class_option.value.decode("ascii")

            if vendor_class_option.option_type.name == "uint32":
                data_value = int(vendor_class_option.value)

            if data_value == "":
                data_value = vendor_class_option.value.decode("ascii")

            if vendor_class_option.code43sub:
                space_name = "space-code43-" + str(vendor_class.id)
                option_name = "code-code43-" + str(vendor_class_option.option_code)

                kea_conf["Dhcp4"]["option-def"].append(
                    {
                        "name": option_name,
                        "code": vendor_class_option.option_code,
                        "type": vendor_class_option.option_type.name,
                        "space": space_name,
                        "array": vendor_class_option.array,
                    }
                )

                vendor_class_def["option-data"].append(
                    {
                        "name": option_name,
                        "space": space_name,
                        "data": str(data_value),
                        "always-send": True,
                    }
                )
            else:
                # can not set reserved dhcp options as own code
                try:
                    rfc_code = DcIpDhcpOption.objects.get(
                        id=vendor_class_option.option_code
                    )
                except DcIpDhcpOption.DoesNotExist:
                    vendor_class_def["option-def"].append(
                        {
                            "name": option_name,
                            "code": vendor_class_option.option_code,
                            "type": vendor_class_option.option_type.name,
                            "array": vendor_class_option.array,
                        }
                    )

                    vendor_class_def["option-data"].append(
                        {
                            "name": option_name,
                            "data": str(data_value),
                            "always-send": True,
                        }
                    )
                else:
                    vendor_class_def["option-data"].append(
                        {
                            "code": vendor_class_option.option_code,
                            "data": str(data_value),
                            "always-send": True,
                        }
                    )

        kea_conf["Dhcp4"]["client-classes"].append(vendor_class_def)

    # remove client-classes if empty (fixes syntax error)
    if len(kea_conf["Dhcp4"]["client-classes"]) <= 0:
        del kea_conf["Dhcp4"]["client-classes"]

    global_dhcp_options = dict()
    global_dhcp_options_reserved = dict()

    for dhcp_option in DcIpDhcpOptionGlobalDefault.objects.all():
        global_dhcp_options[dhcp_option.option.name] = dhcp_option.value

    for dhcp_option in instance.dcipdhcpoptioninstance_set.all():
        global_dhcp_options[dhcp_option.option.name] = dhcp_option.value

    for dhcp_option_name, dhcp_option_value in global_dhcp_options.items():
        kea_conf["Dhcp4"]["option-data"].append(
            {
                "name": dhcp_option_name,
                "data": dhcp_option_value.decode("utf-8"),
            }
        )

    for dhcp_option_name, dhcp_option_value in global_dhcp_options_reserved.items():
        kea_conf["Dhcp4"][dhcp_option_name] = dhcp_option_value

    kea_conf["Dhcp4"]["subnet4"] = list()

    for vlan in vlans:
        networks = vlan.dcipnetwork_set.all()

        if only_network_ids is not None:
            networks = networks.filter(id__in=only_network_ids)

        for network in networks:
            # Skip networks where dhcp is disabled
            if network.dhcp_enabled is False:
                continue

            # Skip ipv6 networks
            if isinstance(network.network, IPv6Network):
                continue

            subnet = dict()

            subnet["subnet"] = str(network.network)

            subnet["id"] = network.id

            if network.dhcp_ipv4_next_server is not None:
                subnet["next-server"] = network.dhcp_ipv4_next_server

            if network.dhcp_ipv4_boot_file_name is not None:
                subnet["boot-file-name"] = network.dhcp_ipv4_boot_file_name

            subnet["pools"] = list()

            if network.dyn_dhcp_enabled is True:
                subnet["pools"].append(
                    {
                        "pool": str(network.network[1])
                        + " - "
                        + str(network.network[-2]),
                    }
                )

            subnet["option-data"] = list()

            if network.router is not None:
                subnet["option-data"].append(
                    {
                        "name": "routers",
                        "data": str(network.router.ip_address),
                    }
                )

            for dhcp_option in network.dcipdhcpoptionipnetwork_set.all():
                subnet["option-data"].append(
                    {
                        "name": dhcp_option.option.name,
                        "data": dhcp_option.value.decode("utf-8"),
                    }
                )

            if len(subnet["option-data"]) == 0:
                del subnet["option-data"]

            subnet["reservations"] = list()

            for reservation in network.dcipaddress_set.exclude(
                mac_address="ff:ff:ff:ff:ff:ff"
            ).exclude(dhcp_enabled=False):
                sn_reservation = dict()

                sn_reservation["hostname"] = reservation.reverse_fqdn
                sn_reservation["hw-address"] = str(reservation.mac_address)
                sn_reservation["ip-address"] = str(reservation.ip_address)

                if reservation.dhcp_ipv4_next_server is not None:
                    sn_reservation["next-server"] = reservation.dhcp_ipv4_next_server

                if reservation.dhcp_ipv4_boot_file_name is not None:
                    sn_reservation["boot-file-name"] = (
                        reservation.dhcp_ipv4_boot_file_name
                    )

                sn_reservation["option-data"] = list()

                if reservation.router is not None:
                    sn_reservation["option-data"].append(
                        {
                            "name": "routers",
                            "data": str(network.router.ip_address),
                        }
                    )

                for dhcp_option in reservation.dcipdhcpoptionipaddress_set.all():
                    sn_reservation["option-data"].append(
                        {
                            "name": dhcp_option.option.name,
                            "data": dhcp_option.value.decode("utf-8"),
                        }
                    )

                if len(sn_reservation["option-data"]) == 0:
                    del sn_reservation["option-data"]

                subnet["reservations"].append(sn_reservation)

            kea_conf["Dhcp4"]["subnet4"].append(subnet)

    return kea_conf


def generate_dhcp6_config(instance: DcInstance, only_network_ids=None):
    kea_conf = json.load(
        open(
            "django_datacenter_manager_cfg_isc_kea/django_datacenter_manager_cfg_isc_kea/templates/django_datacenter_manager_cfg_isc_kea/kea-dhcp6.conf.json"
        )
    )

    return kea_conf

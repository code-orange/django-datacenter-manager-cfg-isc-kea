from django.core.management.base import BaseCommand

from django_datacenter_manager_cfg_isc_kea.django_datacenter_manager_cfg_isc_kea.func import *
from django_datacenter_manager_main.django_datacenter_manager_main.models import (
    DcInstance,
)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("instance_id", type=int)

    def handle(self, *args, **options):
        with open("kea-dhcp4.conf", "w") as kea_config_file:
            json.dump(
                generate_dhcp4_config(
                    DcInstance.objects.get(id=options["instance_id"]),
                ),
                kea_config_file,
                indent=4,
            )

        with open("kea-dhcp6.conf", "w") as kea_config_file:
            json.dump(
                generate_dhcp6_config(
                    DcInstance.objects.get(id=options["instance_id"]),
                ),
                kea_config_file,
                indent=4,
            )
